from __future__ import division, print_function  # Python 2 compatibility

from gym.envs.registration import register
from .spyhunter_env2 import SpyHunter

register(
    id='nesgym/SpyHunter-v1',
    entry_point='nesgym:SpyHunter',
    max_episode_steps=9999999,
    reward_threshold=32000,
    kwargs={},
    nondeterministic=True,
)
