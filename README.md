# Spy Hunter RL Project

## Requirements
- Python 3.7
- [tensorflow](https://github.com/tensorflow/tensorflow)
- [keras-rl](https://github.com/keras-rl/keras-rl)
- [nes-py](https://github.com/Kautenja/nes-py)
- Spy Hunter ROM
- [graphviz](https://www.graphviz.org/) optional - for plotting

Results can be found [here](https://bitbucket.org/nscheirer/spyhunterrl/wiki/Home).

This project is not endorsed by Nintendo in any way and is for educational
purposes only.
