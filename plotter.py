import os, json, subprocess, pdb
from keras.utils import plot_model
from keras.models import load_model, model_from_json
import matplotlib
from matplotlib import pyplot as plt


weight_path = r""

with open(os.path.join(weight_path, "dqn_log.json"), "r") as f:
    log = json.load(f)

plt.figure(figsize=(18, 4))
plt.subplot(131)
plt.plot(log['episode'], log['loss'])
plt.xlabel("Episode")
plt.title("Loss")

plt.subplot(132)
plt.plot(log['episode'], log['episode_reward'])
plt.xlabel("Episode")
plt.title("Reward")

plt.subplot(133)
plt.plot(log['episode'], log['mean_q'])
plt.xlabel("Episode")
plt.title("Mean Q")

plt.figure(figsize=(18, 4))
plt.subplot(121)
plt.plot(log['episode'], log['nb_episode_steps'])
plt.xlabel("Episode")
plt.ylabel("Num of steps")
plt.title("Episode Duration")

plt.subplot(122)
plt.plot(log['episode'], log['mean_eps'])
plt.xlabel("Episode")
plt.title("Mean Epsilon")

with open(os.path.join(weight_path, 'model.json'), 'r') as f:
    model_json = json.load(f)

model = model_from_json(model_json)
plot_model(model, to_file='model.png', show_shapes=True, show_layer_names=False)

if os.path.exists("tmp"):
    namelist = range(0, len(os.listdir("tmp")))
    filenamelist = [os.path.join("tmp", "{}.png".format(i)) for i in namelist]
    convertexepath = r"C:\Program Files\ImageMagick-7.0.9-Q16\magick.exe"
    convertcommand = [convertexepath, "-delay", "10", "-size", str(128) + "x" + str(128)] + filenamelist[0:len(filenamelist):10] + ["anim.gif"]
    print(convertcommand)
    subprocess.call(convertcommand)

plt.show()
