"""An OpenAI Gym interface to the NES game SpyHunter"""
import numpy as np
from nes_py import NESEnv


class SpyHunter(NESEnv):
    """An OpenAI Gym interface to the NES game SpyHunter"""

    def __init__(self):
        """Initialize a new SpyHunter."""
        super(SpyHunter, self).__init__(r'Spy_Hunter\spyhunter.nes')
        # setup any variables to use in the below callbacks here

        self._last_score = 0

        self._skip_start_screen()
        # create a backup state to restore from on subsequent calls to reset
        self._backup()

    def _will_reset(self):
        """Handle any RAM hacking after a reset occurs."""
        # use this method to perform setup before and episode resets.
        # the method returns None
        self._time_last = 0
        pass

    def _did_reset(self):
        """Handle any RAM hacking after a reset occurs."""
        # use this method to access the RAM of the emulator
        # and perform setup for each episode.
        # the method returns None
        self._time_last = self._time
        # idle = np.random.randint(1000)
        # for i in range(0, idle):
        #     self._frame_advance(8)
        #     self._frame_advance(0)
        pass

    def _did_step(self, done):
        """
        Handle any RAM hacking after a step occurs.

        Args:
            done: whether the done flag is set to true

        Returns:
            None

        """
        # No enemy cars
        # self.ram[0x00C6] = 0x00
        # No civilian cars
        # self.ram[0x00C5] = 0x00
        # Unlimited lives
        # self.ram[0x0111] = 0x03
        pass

    def _get_reward(self):
        """Return the reward after a step occurs."""
        return self._status_reward + self._score_reward

    def _get_done(self):
        """Return True if the episode is over, False otherwise."""
        if self._life < 1 and self._status == 6:
            # self.ram[0x0111] = 2
            return True
        # if self._score > 9999000:
        #     return True
        return False

    def _get_info(self):
        """Return the info after a step occurs."""
        return dict(
            status=self._status,
            score=self._score,
            time=self._time,
        )

    def _read_mem_range(self, address, length):
        """
        Read a range of bytes where each byte is a 10's place figure.
        Args:
            address (int): the address to read from as a 16 bit integer
            length: the number of sequential bytes to read
        Note:
            this method is specific to Spy Hunter where three GUI values are stored
            in independent memory slots to save processing time
            - score has 7 10's places
            - time has 3 10's places
        Returns:
            the integer value of this 10's place representation
        """
        return int(''.join(map(str, self.ram[address:address + length])))

    @property
    def _score(self):
        """Return the current player score (0 to 9999990)."""
        # score is represented as a figure with 7 10's places
        return self._read_mem_range(0x0123, 7)

    @property
    def _score_reward(self):
        _reward = self._score - self._last_score
        self._last_score = self._score
        return _reward

    @property
    def _time(self):
        """Return the time left (0 to 999)."""
        # time is represented as a figure with 3 10's places
        return self._read_mem_range(0x011B, 3)

    @property
    def _speed(self):
        """Return the speed of the vehicle"""
        return self.ram[0x00E2]

    @property
    def _life(self):
        """Return the number of remaining lives."""
        return self.ram[0x0111]

    @property
    def _level(self):
        """Return the level of the game."""
        return self.ram[0x00B1]

    @property
    def _status(self):
        """Return the level of the game."""
        return self.ram[0x0084]

    @property
    def _status_reward(self):
        if self._status == 6:
            _reward = -10.
        elif self._status == 5:
            _reward = -5.
        else:
            _reward = 0.
        return _reward

    def _skip_start_screen(self):
        """Press and release start to skip the start screen."""
        # press and release the start button
        self._frame_advance(8)
        self._frame_advance(0)
        # Press start until the game starts
        while self._time in [0, 151515]:
            # press and release the start button
            self._frame_advance(8)
            self._frame_advance(0)
        # set the last time to now
        self._time_last = self._time
        # after the start screen idle to skip some extra frames
        for i in range(1500):
            self._time_last = self._time
            self._frame_advance(8)
            self._frame_advance(0)


# explicitly define the outward facing API for the module
__all__ = [SpyHunter.__name__]
