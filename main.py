from __future__ import division, print_function  # Python 2 compatibility

from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Convolution2D, Permute
from keras.optimizers import Adam, RMSprop
import keras.backend as K

from rl.agents.dqn import DQNAgent
from rl.policy import LinearAnnealedPolicy, BoltzmannQPolicy, EpsGreedyQPolicy
from rl.memory import SequentialMemory
from rl.core import Processor
from rl.callbacks import FileLogger, ModelIntervalCheckpoint

import json
import gym
from PIL import Image
import numpy as np

import nesgym
from nes_py.wrappers import JoypadSpace


# Set to True to Train or False to Test
TRAIN = False
WINDOW_LENGTH = 4
INPUT_SHAPE = (128, 128)


def get_env():
    env = gym.make('nesgym/SpyHunter-v1')
    env = JoypadSpace(env, [
        ['R'], ['R', 'A'], ['R', 'B'],
        ['L'], ['L', 'A'], ['L', 'B'],
        ['U'], ['U', 'A'], ['U', 'B'],
        ['D'], ['D', 'A'], ['D', 'B'],
        ['U', 'R'], ['U', 'R', 'A'], ['U', 'R', 'B'],
        ['U', 'L'], ['U', 'L', 'A'], ['U', 'L', 'B'],
        ['D', 'R'], ['D', 'R', 'A'], ['D', 'R', 'B'],
        ['D', 'L'], ['D', 'L', 'A'], ['D', 'L', 'B'],
        ['A'], ['B'], ['E'], ['NOOP']
    ])
    return env


class FrameProcessor(Processor):
    def process_observation(self, observation):
        assert observation.ndim == 3  # (height, width, channel)
        img = Image.fromarray(observation)
        img = img.resize(INPUT_SHAPE).convert('L')  # resize and convert to grayscale
        processed_observation = np.array(img)
        assert processed_observation.shape == INPUT_SHAPE
        return processed_observation.astype('uint8')  # saves storage in experience memory

    def process_state_batch(self, batch):
        # We could perform this processing step in `process_observation`. In this case, however,
        # we would need to store a `float32` array instead, which is 4x more memory intensive than
        # an `uint8` array. This matters if we store 1M observations.
        processed_batch = batch.astype('float32') / 255.
        return processed_batch

    def process_reward(self, reward):
        return np.clip(reward, -10., 10.)


def start_step_policy(observation):
    return 27


def spyhunter_main():
    env = get_env()

    nb_actions = env.action_space.n

    input_shape = (WINDOW_LENGTH,) + INPUT_SHAPE
    model = Sequential()
    if K.image_data_format() == 'channels_last':
        # (width, height, channels)
        model.add(Permute((2, 3, 1), input_shape=input_shape))
    elif K.image_data_format() == 'channels_first':
        # (channels, width, height)
        model.add(Permute((1, 2, 3), input_shape=input_shape))
    else:
        raise RuntimeError('Unknown image_dim_ordering.')
    model.add(Convolution2D(32, (8, 8), strides=(4, 4)))
    model.add(Activation('relu'))
    model.add(Convolution2D(64, (4, 4), strides=(2, 2)))
    model.add(Activation('relu'))
    model.add(Convolution2D(128, (3, 3), strides=(1, 1)))
    model.add(Activation('relu'))
    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dense(256))
    model.add(Activation('relu'))
    model.add(Dense(128))
    model.add(Activation('relu'))
    model.add(Dense(nb_actions))
    model.add(Activation('linear'))
    print(model.summary())

    # with open("model.json", "w") as json_file:
    #     json.dump(model.to_json(), json_file)

    memory = SequentialMemory(limit=1000000, window_length=WINDOW_LENGTH)
    processor = FrameProcessor()

    policy = LinearAnnealedPolicy(EpsGreedyQPolicy(), attr='eps', value_max=0.2, value_min=0.1, value_test=.05,
                                  nb_episodes=0.1e3)

    dqn = DQNAgent(model=model, nb_actions=nb_actions, policy=policy, memory=memory,
                   processor=processor, nb_steps_warmup=50000, gamma=.99, target_model_update=10000,
                   train_interval=4, delta_clip=1.)
    dqn.compile(Adam(lr=.00025), metrics=['mae'])

    # load old weights for starting point
    # dqn.load_weights('dqn_weights_old.h5f')

    if TRAIN:
        weights_filename = 'dqn_weights.h5f'
        checkpoint_weights_filename = 'dqn_weights_{step}.h5f'
        log_filename = 'dqn_log.json'
        callbacks = [ModelIntervalCheckpoint(checkpoint_weights_filename, interval=250000)]
        callbacks += [FileLogger(log_filename, interval=100)]
        dqn.fit(env, nb_max_episodes=0.25e3, action_repetition=4, callbacks=callbacks, nb_max_start_steps=1000, log_interval=10000, visualize=False)

        # After training is done, we save the final weights one more time.
        dqn.save_weights(weights_filename, overwrite=False)

        print("Training complete...")
        # dqn.test(env, nb_episodes=10, visualize=False)
    else:
        weights_filename = 'weights/final/dqn_weights.h5f'
        dqn.load_weights(weights_filename)
        dqn.test(env, nb_episodes=1, nb_max_start_steps=100, visualize=True, save_visualize=False)


if __name__ == "__main__":
    spyhunter_main()
